package be.rivendale.list;

import be.rivendale.parser.Parser;

public abstract class AbstractParserTest<T extends Enum<?>> {
    protected void assertParsable(String sentence) {
        System.out.format("Parsing '%s'%n", sentence);
        createParser(sentence).parse();
    }

    protected abstract Parser<T> createParser(String sentence);
}
