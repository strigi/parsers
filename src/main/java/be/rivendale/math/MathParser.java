package be.rivendale.math;

import be.rivendale.parser.Parser;

import static be.rivendale.math.MathTokenType.*;

public class MathParser extends Parser<MathTokenType> {
    public MathParser(String sentence) {
        super(new MathLexer(sentence), 1);
    }

    public void parse() {
        operation();
        match(eof);
    }

    private void operation() {
        term();
        while(isOperator()) {
            operator();
            term();
        }
    }

    private boolean isOperator() {
        return isEqual(plus, minus, multiply, divide);
    }

    private void term() {
        if(isEqual(leftParenthesis)) {
            group();
        } else {
            match(number);
        }
    }

    private void group() {
        match(leftParenthesis);
        operation();
        match(rightParenthesis);
    }

    private void operator() {
        match(plus, minus, multiply, divide);
    }
}
