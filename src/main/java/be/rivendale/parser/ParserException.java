package be.rivendale.parser;

public abstract class ParserException extends RuntimeException {
    public ParserException(String message, Object... arguments) {
        super(String.format(message, arguments));
    }
}
