package be.rivendale.parser.lexer;

public class LexerException extends RuntimeException {
    public LexerException(char character, int position) {
        super(String.format("Unexpected character '%c' found at position '%d' in stream", character, position + 1));
    }
}
