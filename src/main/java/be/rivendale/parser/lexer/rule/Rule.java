package be.rivendale.parser.lexer.rule;

import be.rivendale.parser.lexer.Lexer;
import be.rivendale.parser.lexer.Token;

import java.util.function.Function;

public class Rule<TOKEN_TYPE extends Enum<?>> {
    private Function<Lexer<TOKEN_TYPE>, Boolean> prediction;
    private Function<Lexer<TOKEN_TYPE>, Token<TOKEN_TYPE>> execution;

    public Rule(Function<Lexer<TOKEN_TYPE>, Boolean> prediction, Function<Lexer<TOKEN_TYPE>, Token<TOKEN_TYPE>> execution) {
        this.prediction = prediction;
        this.execution = execution;
    }

    boolean predict(Lexer<TOKEN_TYPE> lexer) {
        return prediction.apply(lexer);
    }

    Token<TOKEN_TYPE> execute(Lexer<TOKEN_TYPE> lexer) {
        return execution.apply(lexer);
    }
}