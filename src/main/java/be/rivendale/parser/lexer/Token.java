package be.rivendale.parser.lexer;

public class Token<TOKEN_TYPE extends Enum<?>> {
    public final TOKEN_TYPE type;
    public final int position;
    public final String lexeme;

    public Token(TOKEN_TYPE type, int position) {
        this(type, position, null);
    }

    public Token(TOKEN_TYPE type, int position, String lexeme) {
        this.type = type;
        this.position = position;
        this.lexeme = lexeme;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(type);
        if(lexeme != null) {
            builder.append(String.format(" (%s)", lexeme));
        }
        return builder.toString();
    }
}
