package be.rivendale.parser;

import be.rivendale.parser.lexer.Lexer;
import be.rivendale.parser.lexer.Token;

import java.util.LinkedList;

/**
 * Lookahead buffer for an LL(k) parser with a predefined k.
 * @param <TOKEN_TYPE>
 */
class FixedLookaheadBuffer<TOKEN_TYPE extends Enum<?>> {
    private final Lexer<TOKEN_TYPE> lexer;
    private final LinkedList<Token<TOKEN_TYPE>> buffer = new LinkedList<>();
    private final int k;

    FixedLookaheadBuffer(Lexer<TOKEN_TYPE> lexer, int k) {
        this.lexer = lexer;
        this.k = k;
        fill(lexer, k);
    }

    private void fill(Lexer<TOKEN_TYPE> lexer, int k) {
        for (int i = 0; i < k; i++) {
            buffer.addLast(lexer.next());
        }
    }

    Token<TOKEN_TYPE> peek(int position) {
        if(position < 1 || position > k) {
            throw new InvalidPeekPosition(position, k);
        }
        return buffer.get(position - 1);
    }

    void consume() {
        buffer.removeFirst();
        buffer.addLast(lexer.next());
    }
}
