package be.rivendale.parser;

import be.rivendale.parser.lexer.Lexer;

public abstract class Parser<TOKEN_TYPE extends Enum<?>> {
    private final FixedLookaheadBuffer<TOKEN_TYPE> buffer;

    public Parser(Lexer<TOKEN_TYPE> lexer, int k) {
        buffer = new FixedLookaheadBuffer<>(lexer, k);
    }

    protected void match(TOKEN_TYPE... tokenTypes) {
        for (TOKEN_TYPE tokenType : tokenTypes) {
            if(isEqual(tokenType)) {
                buffer.consume();
                return;
            }
        }
        throw newParserException(1, tokenTypes);
    }

    protected boolean isEqual(TOKEN_TYPE... types) {
        return isEqual(1, types);
    }

    protected boolean isEqual(int position, TOKEN_TYPE... types) {
        for (TOKEN_TYPE type : types) {
            if(type(position) == type) {
                return true;
            }
        }
        return false;
    }

    protected ParserException newParserException(int position, TOKEN_TYPE... expectedTokenTypes) {
        return new TokenMismatchException(buffer.peek(position), expectedTokenTypes);
    }

    private TOKEN_TYPE type(int position) {
        return buffer.peek(position).type;
    }

    public abstract void parse();
}
