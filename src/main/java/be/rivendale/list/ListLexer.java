package be.rivendale.list;

import be.rivendale.parser.lexer.rule.Rule;
import be.rivendale.parser.lexer.rule.RuleBasedLexer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static be.rivendale.list.ListTokenType.*;

public class ListLexer extends RuleBasedLexer<ListTokenType> {
    public ListLexer(String sentence) {
        super(sentence);
    }

    @Override
    protected void populateRules(List<Rule<ListTokenType>> rules) {
        rules.add(new Rule<>(lexer -> lexer.isEqual('['), lexer -> single(leftBracket)));
        rules.add(new Rule<>(lexer -> lexer.isEqual(']'), lexer -> single(rightBracket)));
        rules.add(new Rule<>(lexer -> lexer.isEqual('\0'), lexer -> single(eof)));
        rules.add(new Rule<>(lexer -> lexer.isEqual(','), lexer -> single(comma)));
        rules.add(new Rule<>(lexer -> lexer.isEqual('='), lexer -> single(equals)));
        rules.add(new Rule<>(lexer -> lexer.isAlphabetical(), lexer -> single(variable)));
    }
}
